#pragma once

#include <vector>

#include <Window.h>

#include "Table.h"

class TableWindow : public Window
{
public:
	TableWindow(HINSTANCE hInstance);
	~TableWindow();

	HWND Create(HWND hwnd, HMENU hMenu);

	void SetTable(const Table* table);
	const Table& GetTable() const;

protected:
	virtual LRESULT WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) override;

	LRESULT OnCreate(WPARAM wParam, LPARAM lParam);
	LRESULT OnPaint(WPARAM wParam, LPARAM lParam);
	LRESULT OnVScroll(WPARAM wParam, LPARAM lParam);
	LRESULT OnSize(WPARAM wParam, LPARAM lParam);

	HFONT CreateDefaultFont();
private:
	static WindowClass windowClass;
	static bool isClassRegistered;

	int height;

	HFONT font;
	const Table* table;
};

