#pragma once

#include "Application.h"
#include <gdiplus.h>

class TableViewerWindow;

class TableViewerApplication :
	public Application
{
public:
	TableViewerApplication(HINSTANCE hInstance, LPWSTR lpCmdLine);
	~TableViewerApplication();

	virtual void Initialize(int nCmdShow) override;
	virtual void Finalize() override;
private:
	TableViewerWindow* window;

	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;
};

