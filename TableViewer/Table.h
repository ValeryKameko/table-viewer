#pragma once
#include <wchar.h>
#include <string>
#include <vector>

class Table
{
public:
	Table(int nRows, int nColumns);
	~Table();

	std::wstring& get(int i, int j);
	const std::wstring& get(int i, int j) const;

	int columns() const;
	int rows() const;
private:
	int nColumns;
	int nRows;

	std::vector<std::vector<std::wstring> > strings;
};

