#pragma once

#include "Table.h"
#include <vector>
#include <wchar.h>
#include <string>

class CsvLoader
{
public:
	CsvLoader();
	~CsvLoader();

	Table* LoadTable(const std::wstring& filePath);

	bool ProcessChar(wchar_t ch);
	bool ProcessNormal(wchar_t ch);
	bool ProcessValue(wchar_t ch);
	bool ProcessStringValue(wchar_t ch);
	bool ProcessStringQuote(wchar_t ch);
	bool ProcessCr(wchar_t ch);
private:

	enum CsvParserState {
		CSV_PARSER_STATE_NORMAL,
		CSV_PARSER_STATE_VALUE,
		CSV_PARSER_STATE_STRING_VALUE,
		CSV_PARSER_STATE_STRING_QUOTE
	};

	CsvParserState state;
	std::vector<std::vector<std::wstring> > strings;
	std::vector<std::wstring> row;
	std::wstring string;
};

