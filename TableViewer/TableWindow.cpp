#include "TableWindow.h"


WindowClass TableWindow::windowClass = WindowClass();
bool TableWindow::isClassRegistered = false;


constexpr int MARGIN = 20;
constexpr int MIN_HEIGHT = 30;
constexpr int H_MARGIN = 5;

TableWindow::TableWindow(HINSTANCE hInstance)
	: Window(hInstance), height(0)
{
	if (!isClassRegistered) {
		WNDCLASSEX wndClass = {};
		wndClass.cbClsExtra = 0;
		wndClass.cbWndExtra = 0;
		wndClass.hbrBackground = (HBRUSH)NULL;
		wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		wndClass.hIcon = NULL;
		wndClass.hIconSm = NULL;
		wndClass.lpszMenuName = 0;
		wndClass.style = CS_HREDRAW | CS_VREDRAW;

		windowClass = WindowClass(hInstance, L"TABLE_WINDOW_CLASS", &wndClass);
		windowClass.Register();
		isClassRegistered = true;
	}

	SetWindowClass(&windowClass);
}

TableWindow::~TableWindow()
{
}

HWND TableWindow::Create(HWND parent, HMENU hMenu)
{
	return Window::Create(
		0,
		L"Table window",
		WS_CHILD | WS_VISIBLE | WS_VSCROLL,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		parent, hMenu);
}

void TableWindow::SetTable(const Table* table)
{
	this->table = table;

	height = 0;

	SCROLLINFO si;
	si.fMask = SIF_ALL;
	GetScrollInfo(GetHwnd(), SB_VERT, &si);

	si.nTrackPos = 0;
	si.nPos = 0;
	si.fMask = SIF_POS | SIF_TRACKPOS;

	SetScrollInfo(GetHwnd(), SB_VERT, &si, FALSE);

	RECT rect;
	GetClientRect(GetHwnd(), &rect);
	InvalidateRect(GetHwnd(), &rect, TRUE);
}

const Table& TableWindow::GetTable() const
{
	return *table;
}

LRESULT TableWindow::WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{

	switch (uMsg) {
	case WM_PAINT:
		return OnPaint(wParam, lParam);
	case WM_CREATE:
		return OnCreate(wParam, lParam);
	case WM_VSCROLL:
		return OnVScroll(wParam, lParam);
	case WM_SIZE:
		return OnSize(wParam, lParam);
	}
	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

LRESULT TableWindow::OnCreate(WPARAM wParam, LPARAM lParam)
{
	return DefWindowProc(GetHwnd(), WM_CREATE, wParam, lParam);
}

LRESULT TableWindow::OnPaint(WPARAM wParam, LPARAM lParam)
{	
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(GetHwnd(), &ps);

	RECT clientRect;
	GetClientRect(GetHwnd(), &clientRect);
	FillRect(hdc, &clientRect, (HBRUSH)GetStockObject(WHITE_BRUSH));

	HGDIOBJ oldFont = SelectObject(hdc, font);
	HBRUSH brush = CreateSolidBrush(RGB(255, 0, 0));
	HGDIOBJ oldBrush = SelectObject(hdc, brush);
	HPEN pen = CreatePen(PS_SOLID, 2, RGB(255, 0, 0));

	HGDIOBJ oldPen;

	RECT boundRect;

	if (table) {
		SCROLLINFO si;
		si.fMask = SIF_ALL;
		GetScrollInfo(GetHwnd(), SB_VERT, &si);

		int offset = max(0, int((height - (clientRect.right - clientRect.left)) * ((si.nPos - si.nMin) / (float)(si.nMax - si.nMin))));

		RECT rect;
		GetClientRect(GetHwnd(), &rect);

		int sumW = rect.right - rect.left;
		int w = (sumW - (table->columns() + 1) * MARGIN + table->columns() - 1) / table->columns();
		int sumH = MARGIN;

		int y = sumH - offset;
		for (int i = 0; i < table->rows(); i++) {
			if (i == 0) {
				oldPen = SelectObject(hdc, pen);
				MoveToEx(hdc, MARGIN / 2, y - MARGIN / 2, NULL);
				LineTo(hdc, sumW - MARGIN / 2, y - MARGIN / 2);
				SelectObject(hdc, oldPen);
			}
			int x = MARGIN;
			int h = 0;

			for (int j = 0; j < table->columns(); j++) {
				const std::wstring& str = table->get(i, j);

				boundRect.left = 0;
				boundRect.right = w;
				boundRect.top = 0;
				boundRect.bottom = 0;

				DrawText(
					hdc,
					str.c_str(),
					str.length(),
					&boundRect,
					DT_CALCRECT | DT_CENTER | DT_VCENTER | DT_WORDBREAK);

				h = max(h, boundRect.bottom - boundRect.top);
			}
			h += 2 * H_MARGIN;

			oldPen = SelectObject(hdc, pen);
			MoveToEx(hdc, x - MARGIN / 2, y - MARGIN / 2, NULL);
			LineTo(hdc, x - MARGIN / 2, y + h + MARGIN / 2);
			SelectObject(hdc, oldPen);

			for (int j = 0; j < table->columns(); j++) {
				const std::wstring& str = table->get(i, j);

				boundRect.left = x;
				boundRect.right = x + w;
				boundRect.top = y + H_MARGIN;
				boundRect.bottom = y + h - H_MARGIN;
				
				oldPen = SelectObject(hdc, pen);
				MoveToEx(hdc, boundRect.right + MARGIN / 2, y - MARGIN / 2, NULL);
				LineTo(hdc, boundRect.right + MARGIN / 2, y + h + MARGIN / 2);
				
				MoveToEx(hdc, boundRect.left - MARGIN / 2, y + h + MARGIN / 2, NULL);
				LineTo(hdc, boundRect.right + MARGIN / 2, y + h + MARGIN / 2);
				SelectObject(hdc, oldPen);

				DrawText(
					hdc,
					str.c_str(),
					str.length(),
					&boundRect,
					DT_CENTER | DT_VCENTER | DT_WORDBREAK);

				x += MARGIN + w;
			}

			y += h + MARGIN;
		}

		if (height == 0)
			height = y;
	}

	SelectObject(hdc, oldBrush);
	SelectObject(hdc, oldFont);
	DeleteObject(brush);
	EndPaint(GetHwnd(), &ps);

	return DefWindowProc(GetHwnd(), WM_PAINT, wParam, lParam);
}

LRESULT TableWindow::OnVScroll(WPARAM wParam, LPARAM lParam)
{
	SCROLLINFO si;
	si.cbSize = sizeof(SCROLLINFO);
	si.fMask = SIF_ALL;

	GetScrollInfo(GetHwnd(), SB_VERT, &si);

	int prevYPos = si.nPos;
	switch (LOWORD(wParam)) {
	case SB_TOP:
		si.nPos = si.nMin;
		break;
	case SB_BOTTOM:
		si.nPos = si.nMax;
		break;
	case SB_LINEUP:
		si.nPos -= 1;
		break;
	case SB_LINEDOWN:
		si.nPos += 1;
		break; 
	case SB_PAGEUP:
		si.nPos -= si.nPage;
		break;
	case SB_PAGEDOWN:
		si.nPos += si.nPage;
		break;
	case SB_THUMBTRACK:
		si.nPos = si.nTrackPos;
		break;
	}

	SetScrollInfo(GetHwnd(), SB_VERT, &si, TRUE);
	ScrollWindow(GetHwnd(), 0, prevYPos - si.nPos, nullptr, nullptr);

	RECT rect;
	GetClientRect(GetHwnd(), &rect);
	InvalidateRect(GetHwnd(), &rect, TRUE);

	return DefWindowProc(GetHwnd(), WM_VSCROLL, wParam, lParam);
}

LRESULT TableWindow::OnSize(WPARAM wParam, LPARAM lParam)
{
	RECT rect;
	GetClientRect(GetHwnd(), &rect);
	InvalidateRect(GetHwnd(), &rect, TRUE);

	return DefWindowProc(GetHwnd(), WM_SIZE, wParam, lParam);
}

HFONT TableWindow::CreateDefaultFont()
{
	LOGFONT logFont;
	logFont.lfHeight = 14;
	logFont.lfWidth = 0;
	logFont.lfEscapement = 0;
	logFont.lfOrientation = 0;
	logFont.lfWeight = FW_BOLD;
	logFont.lfItalic = FALSE;
	logFont.lfUnderline = FALSE;
	logFont.lfStrikeOut = FALSE;
	logFont.lfCharSet = DEFAULT_CHARSET;
	logFont.lfOutPrecision = OUT_CHARACTER_PRECIS;
	logFont.lfClipPrecision = CLIP_CHARACTER_PRECIS;
	logFont.lfQuality = ANTIALIASED_QUALITY;
	logFont.lfPitchAndFamily = FF_DONTCARE;
	lstrcpyW(logFont.lfFaceName, L"Times New Roman");

	return CreateFontIndirect(&logFont);
}