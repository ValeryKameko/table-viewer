#pragma once
#include <Window.h>
#include "Table.h"
#include "TableWindow.h"

class TableViewerWindow :
	public Window
{
public:
	TableViewerWindow(HINSTANCE hInstance);
	~TableViewerWindow();

	HWND Create();

protected:
	virtual LRESULT WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) override;

	LRESULT OnPaint(WPARAM wParam, LPARAM lParam);
	LRESULT OnCreate(WPARAM wParam, LPARAM lParam);
	LRESULT OnCommand(WPARAM wParam, LPARAM lParam);
	LRESULT OnSize(WPARAM wParam, LPARAM lParam);

	LRESULT OnOpenFile();

	HFONT CreateDefaultFont();
	void AdjustTableWindowSize();

private:
	static WindowClass windowClass;
	static bool isClassRegistered;

	HMENU menu;
	HFONT font;
	Table* table;
	TableWindow* tableWindow;
};

