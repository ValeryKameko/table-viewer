#include "CsvLoader.h"
#include <fstream>
#include <algorithm>


constexpr size_t BUFFER_SIZE = 1 << 12;
constexpr wchar_t DELIMITER = ',';
constexpr wchar_t CRLF = L'\n';
constexpr wchar_t QUOTE = L'"';


CsvLoader::CsvLoader()
{
}


CsvLoader::~CsvLoader()
{
}

Table* CsvLoader::LoadTable(const std::wstring& filePath)
{
	std::wifstream file(filePath);
	if (!file)
		return nullptr;

	state = CSV_PARSER_STATE_NORMAL;
	strings.clear();
	row.clear();
	string = L"";

	bool result;
	wchar_t ch;
	while (file.get(ch)) {
		if (!(result = ProcessChar(ch)))
			break;
	}
	file.close();
	if (!result)
		return nullptr;

	if (state == CSV_PARSER_STATE_STRING_VALUE)
		return nullptr;
	else {
		row.push_back(string);
		strings.push_back(row);
	}

	int nRows = strings.size();
	auto vectorSizeComparator = [](const std::vector<std::wstring> & a, const std::vector<std::wstring> & b) { 
		return a.size() < b.size(); 
	};
	int nColumns = std::max_element(strings.begin(), strings.end(), vectorSizeComparator)->size();

	Table* table = new Table(nRows, nColumns);
	for (int i = 0; i < nRows; i++) {
		for (int j = 0; j < (int)strings[i].size(); j++) {
			table->get(i, j) = strings[i][j];
		}
	}
	return table;
}

bool CsvLoader::ProcessChar(wchar_t ch)
{
	switch (state) {
	case CSV_PARSER_STATE_NORMAL:
		return ProcessNormal(ch);
	case CSV_PARSER_STATE_VALUE:
		return ProcessValue(ch);
	case CSV_PARSER_STATE_STRING_VALUE:
		return ProcessStringValue(ch);
	case CSV_PARSER_STATE_STRING_QUOTE:
		return ProcessStringQuote(ch);
	}
	return false;
}

bool CsvLoader::ProcessNormal(wchar_t ch)
{
	if (ch == CRLF) {
		string = L"";
		row.push_back(string);
		strings.push_back(row);
		row.clear();

		state = CSV_PARSER_STATE_NORMAL;
		return true;
	}
	else if (ch == DELIMITER) {
		string = L"";
		row.push_back(string);
		state = CSV_PARSER_STATE_NORMAL;
		return true;
	}
	else if (ch == QUOTE) {
		state = CSV_PARSER_STATE_STRING_VALUE;
		return true;
	}
	else {
		string += ch;
		state = CSV_PARSER_STATE_VALUE;
		return true;
	}
}

bool CsvLoader::ProcessValue(wchar_t ch)
{
	if (ch == QUOTE) {
		return false;
	}
	else if (ch == DELIMITER) {
		row.push_back(string);
		string = L"";
		state = CSV_PARSER_STATE_NORMAL;
		return true;
	}
	else if (ch == CRLF) {
		row.push_back(string);
		strings.push_back(row);
		row.clear();
		string = L"";

		state = CSV_PARSER_STATE_NORMAL;
		return true;
	}
	else {
		string += ch;
		state = CSV_PARSER_STATE_VALUE;
		return true;
	}
}

bool CsvLoader::ProcessStringValue(wchar_t ch)
{
	if (ch == QUOTE) {
		state = CSV_PARSER_STATE_STRING_QUOTE;
		return true;
	}
	else {
		string += ch;
		state = CSV_PARSER_STATE_STRING_VALUE;
		return true;
	}
}

bool CsvLoader::ProcessStringQuote(wchar_t ch)
{
	if (ch == DELIMITER) {
		row.push_back(string);
		string = L"";
		state = CSV_PARSER_STATE_NORMAL;
		return true;
	}
	else if (ch == CRLF) {
		row.push_back(string);
		strings.push_back(row);
		string = L"";
		return true;
	}
	else if (ch == QUOTE) {
		string += ch;
		state = CSV_PARSER_STATE_STRING_VALUE;
		return true;
	}
	else
		return false;
}
