#include <windows.h>
#include "TableViewerApplication.h"

int WINAPI wWinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPWSTR lpCmdLine,
	int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);

	TableViewerApplication app(hInstance, lpCmdLine);

	app.Initialize(nCmdShow);

	int result = app.RunMessageLoop();

	app.Finalize();

	return result;
}