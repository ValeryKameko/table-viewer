#include "Table.h"


Table::Table(int nRows, int nColumns)
{
	this->nRows = nRows;
	this->nColumns = nColumns;
	strings.resize(nRows);
	for (auto &row : strings) {
		row.resize(nColumns);
	}
}

Table::~Table()
{
	strings.clear();
}

std::wstring& Table::get(int i, int j)
{
	return strings[i][j];
}

const std::wstring& Table::get(int i, int j) const
{
	return strings[i][j];
}

int Table::columns() const
{
	return nColumns;
}

int Table::rows() const
{
	return nRows;
}
