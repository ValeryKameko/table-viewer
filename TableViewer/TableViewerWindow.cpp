#include "TableViewerWindow.h"
#include "resource.h"
#include "CsvLoader.h"

#include <WindowClass.h>

#include <gdiplus.h>

constexpr SIZE_T FILE_PATH_BUFFER_SIZE = 512;

bool TableViewerWindow::isClassRegistered = false;

WindowClass TableViewerWindow::windowClass = WindowClass();

TableViewerWindow::TableViewerWindow(HINSTANCE hInstance)
	: Window(hInstance), table(nullptr)
{
	if (!isClassRegistered) {
		WNDCLASSEX wndClass = {};
		wndClass.cbClsExtra = 0;
		wndClass.cbWndExtra = 0;
		wndClass.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
		wndClass.hIconSm = wndClass.hIcon;
		wndClass.lpszMenuName = 0;
		wndClass.style = CS_HREDRAW | CS_VREDRAW;

		windowClass = WindowClass(hInstance, L"MAIN_WINDOW_CLASS", &wndClass);
		windowClass.Register();
		isClassRegistered = true;
	}

	SetWindowClass(&windowClass);
}


TableViewerWindow::~TableViewerWindow()
{
	DeleteObject(font);
}

HWND TableViewerWindow::Create()
{
	return Window::Create(
		WS_EX_OVERLAPPEDWINDOW,
		L"Application",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		nullptr, nullptr);
}

LRESULT TableViewerWindow::WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg) {
	case WM_CLOSE:
		PostQuitMessage(0);
		break;
	case WM_CREATE:
		return OnCreate(wParam, lParam);
	case WM_SIZE:
		return OnSize(wParam, lParam);
	case WM_PAINT:
		return OnPaint(wParam, lParam);
	case WM_COMMAND:
		return OnCommand(wParam, lParam);
	}
	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

LRESULT TableViewerWindow::OnPaint(WPARAM wParam, LPARAM lParam)
{
	return DefWindowProc(GetHwnd(), WM_PAINT, wParam, lParam);
}

LRESULT TableViewerWindow::OnCreate(WPARAM wParam, LPARAM lParam)
{
	tableWindow = new TableWindow(GetHInstance());
	tableWindow->Create(GetHwnd(), (HMENU)222);
	AdjustTableWindowSize();

	menu = LoadMenu(NULL, MAKEINTRESOURCE(IDR_MAIN_MENU));
	SetMenu(this->GetHwnd(), menu);
	return 0;
}

LRESULT TableViewerWindow::OnCommand(WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{
	case ID_FILE_OPEN:
		return OnOpenFile();
	case ID_FILE_QUIT:
		PostQuitMessage(0);
		return 0;
	}
	return 0;
}

LRESULT TableViewerWindow::OnSize(WPARAM wParam, LPARAM lParam)
{
	AdjustTableWindowSize();
	return 0;
}

LRESULT TableViewerWindow::OnOpenFile()
{
	LPCWSTR filePathBuffer = (LPCWSTR)GlobalAlloc(GPTR, FILE_PATH_BUFFER_SIZE);

	OPENFILENAME of = { };
	of.lStructSize = sizeof(OPENFILENAME);
	of.hwndOwner = this->GetHwnd();
	of.hInstance = nullptr;
	of.lpstrFilter = L"CSV file (*.csv, *.txt)\0*.csv;*txt\0";
	of.lpstrCustomFilter = nullptr;
	of.nMaxCustFilter = 0;
	of.nFilterIndex = 1;
	of.lpstrFile = (LPWSTR)filePathBuffer;
	of.nMaxFile = FILE_PATH_BUFFER_SIZE;
	of.lpstrFileTitle = nullptr;
	of.nMaxFileTitle = 0;
	of.lpstrInitialDir = nullptr;
	of.lpstrTitle = L"Choose file with table";
	of.Flags = OFN_ENABLESIZING | OFN_EXPLORER | OFN_FILEMUSTEXIST; //| OFN_HIDEREADONLY;
	of.nFileOffset = 0;
	of.nFileExtension = 0;
	of.lpstrDefExt = L"csv";
	of.lCustData = 0;
	of.lpfnHook = nullptr;
	of.lpTemplateName = nullptr;
	of.FlagsEx = 0;

	if (GetOpenFileName(&of)) {
		CsvLoader* loader = new CsvLoader();
		Table* newTable = loader->LoadTable(filePathBuffer);
		if (newTable == nullptr) {
			MessageBox(this->GetHwnd(), L"Cannot open file", L"Error", MB_OK | MB_ICONERROR);
		}
		else {
			tableWindow->SetTable(newTable);
			if (table != nullptr) {
				delete table;
				table = nullptr;
			}
			table = newTable;
		}
	}
	return 0;
}

void TableViewerWindow::AdjustTableWindowSize()
{
	RECT rect;
	GetClientRect(GetHwnd(), &rect);
	SetWindowPos(tableWindow->GetHwnd(), NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_NOZORDER | SWP_NOREDRAW | SWP_NOCOPYBITS);
}
