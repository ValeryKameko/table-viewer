#include "TableViewerApplication.h"
#include "TableViewerWindow.h"

#include <WindowClass.h>
#include <Window.h>

#include <objidl.h>
#include <gdiplus.h>


TableViewerApplication::TableViewerApplication(HINSTANCE hInstance, LPWSTR lpCmdLine)
	: Application(hInstance, lpCmdLine)
{
}

TableViewerApplication::~TableViewerApplication()
{
}

void TableViewerApplication::Initialize(int nCmdShow)
{
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	window = new TableViewerWindow(hInstance);
	window->Create();
	window->Show(nCmdShow);
}

void TableViewerApplication::Finalize()
{
	Gdiplus::GdiplusShutdown(gdiplusToken);
}
