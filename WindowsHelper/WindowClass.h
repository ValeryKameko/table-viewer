#pragma once

#include <windows.h>

class WindowClass
{
	friend class Window;
public:
	WindowClass(
		HINSTANCE hInstance,
		LPCWSTR className,
		LPWNDCLASSEX wndClass);
	WindowClass();

	~WindowClass();

	ATOM Register();

	const ATOM& GetAtom() const;
private:

	ATOM atom;
	WNDCLASSEX wndClass;
};

