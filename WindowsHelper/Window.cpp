#include "Window.h"


Window::Window(HINSTANCE hInstance, const WindowClass* windowClass)
{
	this->hInstance = hInstance;
	this->windowClass = *windowClass;
}

Window::Window(HINSTANCE hInstance)
{
	this->hInstance = hInstance;
}

Window::~Window()
{
}

HWND Window::Create(
	DWORD dwExStyle,
	LPCWSTR windowName,
	DWORD dwStyle,
	int x, int y,
	int nWidth, int nHeight,
	HWND parent,
	HMENU hMenu)
{
	return hwnd = CreateWindowEx(
		dwExStyle,
		windowClass.wndClass.lpszClassName,
		windowName,
		dwStyle,
		x, y,
		nWidth, nHeight,
		parent,
		hMenu,
		hInstance,
		this);
}

BOOLEAN Window::Show(int nCmdShow)
{
	return ShowWindow(hwnd, nCmdShow);
}

HWND Window::GetHwnd() const
{
	return hwnd;
}

LRESULT Window::WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

LRESULT Window::MainWndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	Window* window = nullptr;

	if (uMsg == WM_CREATE) {
		CREATESTRUCT* cs = reinterpret_cast<CREATESTRUCT*>(lParam);
		window = reinterpret_cast<Window*>(cs->lpCreateParams);

		SetWindowLongPtr(hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(window));
		window->hwnd = hwnd;
	}

	window = reinterpret_cast<Window*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));

	if (window)
		return window->WndProc(hwnd, uMsg, wParam, lParam);
	else
		return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

void Window::SetWindowClass(const WindowClass* windowClass)
{
	this->windowClass = *windowClass;
}

HINSTANCE Window::GetHInstance() const
{
	return hInstance;
}
