#pragma once
#include <windows.h>
#include "WindowClass.h"

class Window
{
	friend class WindowClass;
public:
	Window(HINSTANCE hInstance, const WindowClass *windowClass);
	Window(HINSTANCE hInstance);
	~Window();

	HWND Create(
		DWORD dwExStyle,
		LPCWSTR windowName,
		DWORD dwStyle,
		int x, int y,
		int nWidth, int nHeight,
		HWND parent,
		HMENU hMenu);
	BOOLEAN Show(int nCmdShow);

	HWND GetHwnd() const;

protected:
	virtual LRESULT WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	void SetWindowClass(const WindowClass* windowClass);

	HINSTANCE GetHInstance() const;

private:
	static LRESULT CALLBACK MainWndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	WindowClass windowClass;
	HINSTANCE hInstance;
	HWND hwnd;
};

