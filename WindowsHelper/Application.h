#pragma once
#include <Windows.h>

class Application
{
public:
	Application(HINSTANCE hInstance, LPWSTR lpCmdLine);
	~Application();

	int RunMessageLoop();

	virtual void Initialize(int nCmdShow) = 0;
	virtual void Finalize() = 0;
protected:
	HINSTANCE hInstance;
	LPWSTR lpCmdLine;
};

