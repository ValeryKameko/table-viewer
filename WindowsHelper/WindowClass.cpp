#include "WindowClass.h"
#include "Window.h"


WindowClass::WindowClass(
	HINSTANCE hInstance,
	LPCWSTR className,
	LPWNDCLASSEX wndClass)
{
	this->wndClass = *wndClass;

	this->wndClass.cbSize = sizeof(WNDCLASSEX);
	this->wndClass.lpfnWndProc = Window::MainWndProc;
	this->wndClass.lpszClassName = className;
	this->wndClass.hInstance = hInstance;
}

WindowClass::~WindowClass()
{
}

ATOM WindowClass::Register()
{
	return atom = RegisterClassEx(&wndClass);
}

const ATOM& WindowClass::GetAtom() const
{
	return atom;
}

WindowClass::WindowClass()
{
}
