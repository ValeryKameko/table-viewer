#include "Application.h"



Application::Application(
	HINSTANCE hInstance,
	LPWSTR lpCmdLine)
{
	this->hInstance = hInstance;
	this->lpCmdLine = lpCmdLine;
}


Application::~Application()
{
}

int Application::RunMessageLoop()
{
	MSG msg = {};
	BOOL bResult;
	while (bResult = GetMessage(&msg, NULL, 0, 0)) {
		if (bResult == -1)
			return -1;
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return (int)msg.wParam;
}
